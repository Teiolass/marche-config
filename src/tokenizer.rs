use std::{collections::VecDeque, fmt::Display};

#[derive(Debug)]
pub enum Keyword {
    Set,
    If,
    True,
    False,
}

#[derive(Debug)]
pub enum Puntuaction {
    Char(char),
}

#[derive(Debug)]
pub enum Literal {
    SignedInteger(i64),
    UnsignedInteger(u64),
    Float(f64),
    String(String),
    Boolean(bool),
}

#[derive(Debug)]
pub enum TokenType {
    Keyword(Keyword),
    Puntuaction(Puntuaction),
    Identifier(String),
    Literal(Literal),
    Comment,
}

#[derive(Debug)]
pub struct Token {
    pub content: TokenType,
    pub position: TextPosition,
}

pub struct Tokenizer<'a> {
    source: CharacterSource<'a>,
    peeked: VecDeque<Token>,
}

impl<'a> Tokenizer<'a> {
    pub fn new(text: &'a str) -> Self {
        Self {
            source: CharacterSource::new(text),
            peeked: VecDeque::new(),
        }
    }

    pub fn peek(&mut self) -> Option<&Token> {
        if self.peeked.front().is_none() {
            if let Some(token) = parse_next(&mut self.source) {
                self.peeked.push_back(token);
            } else {
                return None;
            }
        }
        self.peeked.front()
    }

    pub fn next(&mut self) -> Option<Token> {
        if !self.peeked.is_empty() {
            self.peeked.pop_front()
        } else {
            parse_next(&mut self.source)
        }
    }
}

#[inline]
fn parse_next<'a>(source: &mut CharacterSource<'a>) -> Option<Token> {
    let mut new_line = false;
    let position = source.position;
    while let Some(c) = source.peek() {
        match c {
            ' ' | '\t' | '\r' => {
                source.next();
            }
            '\n' => {
                source.next();
                new_line = true;
            }
            _ => break,
        }
    }
    if new_line {
        let token = Token {
            content: TokenType::Puntuaction(Puntuaction::Char('\n')),
            position,
        };
        return Some(token);
    }
    let position = source.position;
    let content: TokenType = match source.peek() {
        Some(c) if c.is_numeric() => {
            let mut buffer = String::with_capacity(128);
            push_integer(&mut buffer, source);
            if source.peek() == Some('.') {
                source.next();
                buffer.push('.');
                push_integer(&mut buffer, source);
                let number: f64 = buffer.parse().expect("Unparsable float");
                TokenType::Literal(Literal::Float(number))
            } else {
                let number: u64 = buffer.parse().expect("Number too big");
                TokenType::Literal(Literal::UnsignedInteger(number))
            }
        }
        Some('-') => {
            source.next();
            let mut result = None;
            if let Some(peek) = source.peek() {
                if peek.is_numeric() {
                    let mut buffer = String::with_capacity(32);
                    push_integer(&mut buffer, source);
                    if source.peek() == Some('.') {
                        source.next();
                        buffer.push('.');
                        push_integer(&mut buffer, source);
                        let number: f64 = buffer.parse().expect("Unparsable float");
                        result = Some(TokenType::Literal(Literal::Float(-number)));
                    } else {
                        let number: i64 = buffer.parse().expect("Number too big");
                        result = Some(TokenType::Literal(Literal::SignedInteger(-number)));
                    }
                }
            }
            result.unwrap_or(TokenType::Puntuaction(Puntuaction::Char('-')))
        }
        Some('=') => {
            source.next();
            TokenType::Puntuaction(Puntuaction::Char('='))
        }
        Some('"') => {
            source.next();
            let mut buffer = String::with_capacity(256);
            loop {
                if let Some(c) = source.next() {
                    match c {
                        '"' => break TokenType::Literal(Literal::String(buffer)),
                        '\\' => {
                            if let Some(c) = source.next() {
                                buffer.push(c);
                            } else {
                                panic!("String literal terminated with escaped nothing");
                            }
                        }
                        _ => buffer.push(c),
                    }
                }
            }
        }
        Some('#') => {
            while let Some(c) = source.next() {
                if c == '\n' {
                    break;
                }
            }
            TokenType::Comment
        }
        Some(c) if c.is_ascii_alphabetic() => {
            let mut buf = String::with_capacity(32);
            while let Some(c) = source.peek() {
                if c.is_ascii_alphanumeric() || c == '_' {
                    buf.push(c);
                    source.next();
                } else {
                    break;
                }
            }
            if let Some(keyword) = match_keyword(&buf) {
                let token = match keyword {
                    Keyword::True => TokenType::Literal(Literal::Boolean(true)),
                    Keyword::False => TokenType::Literal(Literal::Boolean(false)),
                    _ => TokenType::Keyword(keyword),
                };
                token
            } else {
                buf.shrink_to_fit();
                TokenType::Identifier(buf)
            }
        }
        None => return None,
        _ => panic!("Unexpected character"),
    };
    let token = Token { content, position };
    Some(token)
}

fn push_integer<'a>(buffer: &mut String, source: &mut CharacterSource<'a>) {
    while let Some(c) = source.peek() {
        if c.is_numeric() {
            buffer.push(c);
            source.next();
        } else {
            break;
        }
    }
}

fn match_keyword(s: &str) -> Option<Keyword> {
    match s {
        "set" => Some(Keyword::Set),
        "if" => Some(Keyword::If),
        "false" => Some(Keyword::False),
        "true" => Some(Keyword::True),
        _ => None,
    }
}

#[derive(Debug)]
struct CharacterSource<'a> {
    text: &'a [u8],
    index: usize,
    buffer: Option<char>,
    position: TextPosition,
}

#[derive(Copy, Clone, Debug)]
pub struct TextPosition {
    pub line: usize,
    pub offset: usize,
}

impl Display for TextPosition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", self.line, self.offset)
    }
}

impl<'a> CharacterSource<'a> {
    fn new(text: &'a str) -> Self {
        let position = TextPosition { line: 1, offset: 1 };
        CharacterSource {
            text: text.as_bytes(),
            index: 0,
            buffer: None,
            position,
        }
    }

    fn peek(&mut self) -> Option<char> {
        if self.buffer.is_some() {
            return self.buffer;
        }
        if self.index < self.text.len() {
            let c = self.text[self.index] as char;
            self.buffer = Some(c);
            Some(c)
        } else {
            None
        }
    }

    fn next(&mut self) -> Option<char> {
        if let Some(c) = self.buffer {
            self.index += 1;
            if c == '\n' {
                self.position.line += 1;
                self.position.offset = 1;
            } else {
                self.position.offset += 1;
            }
            return std::mem::take(&mut self.buffer);
        }
        if self.index < self.text.len() {
            let c = self.text[self.index] as char;
            self.index += 1;
            if c == '\n' {
                self.position.line += 1;
                self.position.offset = 1;
            } else {
                self.position.offset += 1;
            }
            Some(c)
        } else {
            None
        }
    }
}
