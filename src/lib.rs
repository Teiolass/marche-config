mod tokenizer;

use std::collections::HashMap;

pub use tokenizer::*;

#[derive(Debug)]
pub enum Value {
    SignedInteger(i64),
    UnsignedInteger(u64),
    String(String),
    Boolean(bool),
    Float(f64),
}

#[derive(Debug, Default)]
pub struct Info {
    pub name_map: HashMap<String, Value>,
}

pub fn interpret_program(mut tokenizer: Tokenizer) -> Info {
    let mut info = Info::default();
    while let Some(token) = tokenizer.next() {
        match token.content {
            TokenType::Keyword(Keyword::Set) => {
                let token = if let Some(token) = tokenizer.next() {
                    token
                } else {
                    panic!()
                };
                let ident = if let TokenType::Identifier(ident) = token.content {
                    ident
                } else {
                    panic!()
                };
                if let Some(token) = tokenizer.next() {
                    if !matches!(
                        token.content,
                        TokenType::Puntuaction(Puntuaction::Char('='))
                    ) {
                        panic!()
                    }
                }
                let expression = interpret_expression(&mut tokenizer);
                info.name_map.insert(ident, expression);
            }
            TokenType::Puntuaction(Puntuaction::Char('\n')) => {}
            TokenType::Comment => {}
            _ => {
                panic!()
            }
        }
    }
    return info;
}

fn interpret_expression(tokenizer: &mut Tokenizer) -> Value {
    let token = if let Some(token) = tokenizer.next() {
        token
    } else {
        panic!()
    };
    let value = match token.content {
        TokenType::Literal(literal) => evaluate_literal(literal),
        _ => {
            println!("Error at {}", token.position);
            println!("Expecting a literal token, but we found this:\n{token:?}");
            panic!()
        }
    };
    let token = tokenizer.next();
    match token.map(|x| x.content) {
        Some(TokenType::Puntuaction(Puntuaction::Char('\n'))) | Some(TokenType::Comment) | None => {
            // this is ok
        }
        _ => panic!(),
    }
    return value;
}

fn evaluate_literal(literal: Literal) -> Value {
    use Value::*;
    match literal {
        Literal::SignedInteger(x) => SignedInteger(x),
        Literal::UnsignedInteger(x) => UnsignedInteger(x),
        Literal::Float(x) => Float(x),
        Literal::String(x) => String(x),
        Literal::Boolean(x) => Boolean(x),
    }
}
