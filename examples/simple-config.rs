use marche_config::*;

fn main() {
    let input = r##"
set a = 4
# this is just a comment
set b = false # another comment
set c = "hello"
   
set d = -2
set e = 2.718
    "##;
    let tokenizer = Tokenizer::new(&input);
    let info = interpret_program(tokenizer);
    dbg!(info);
}
